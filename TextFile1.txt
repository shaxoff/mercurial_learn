����������: ���� ������� ������������, ��� �� ����������� ������ Mercurial ����, ��� 0,6. ������� �������, ���� � ��� ���� 0.6-�����, ��� ����������� �� ����� �������� ��� ���. ��������� ��� ����������� ����������� ��� Mercurial 0.9.1. 
�������� ��������� � Mercurial ���������� hg. ������ Mercurial-������� ���������� � hg, ����� ���� ��� ��� �������, ����� ���� - ��������������� ����� � ���������. 
����� ����, ��� Mercurial ��� ����������, ���������� ������ ������ hg � ��������� ������, � ��������� ��������� ��������� �� �������� ���������� ������������� ��������� ������: 

$ hg
Mercurial Distributed SCM

basic commands (use "hg help" for the full list or option "-v" for details):

 add        add the specified files on the next commit
 annotate   show changeset information per file line
 clone      make a copy of an existing repository
(...)
���� ����� �� ����������, �� ���-�� �� ��� � ����������, � ��� ����� ��������� � InstallTroubleshooting. 
����� ����������, ����� ������ Mercurial ������������, ����������� ����� version: 

$ hg version
Mercurial Distributed SCM (version 0.9.1)

Copyright (C) 2005 Matt Mackall <mpm@selenic.com>
This is free software; see the source for copying conditions. There is NO
warranty; not even for MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.